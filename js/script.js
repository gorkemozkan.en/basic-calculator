// Selecting DOM Elements
const billEntry = document.querySelector('.bill-entry');
const peopleEntry = document.querySelector('.people-entry');
const customEntry = document.querySelector('.custom-entry');
const resetButton = document.querySelector('.reset-button');
const tipResult = document.querySelector('.tip-result');
const totalResult = document.querySelector('.total-result');
const digits = document.querySelectorAll('.digit');
const zeroError = document.querySelector('.error-header');

// Initializing Default Values
let tipValue = 0;
let billValue = 0.15;
let peopleValue = 1;

const calculateTipAmount = () => {
    let tipFormula = (billValue / peopleValue) * tipValue;
    let totalFormula = tipFormula + billValue / peopleValue;
    tipResult.innerHTML = tipFormula.toFixed(2);
    totalResult.innerHTML = totalFormula.toFixed(2);
};

const handleDigit = (event) => {
    digits.forEach((el) => {
        el.classList.remove('selected');
        if (event.target.innerHTML === el.innerHTML) {
            el.classList.add('selected');
            tipValue = parseFloat(el.innerHTML) / 100;
        }
    });
    removeCustomEntry();
    calculateTipAmount();
};

const handleCustomTip = () => {
    if (customEntry.value) {
        tipValue = Number(customEntry.value / 100);
    }
    calculateTipAmount();
};

const handlePeople = () => {
    if (peopleEntry.value > 0) {
        peopleEntry.classList.remove('border-error');
        zeroError.classList.add('hidden');
        peopleValue = Number(peopleEntry.value);
    }
    if (peopleEntry.value <= 0) {
        peopleEntry.classList.add('border-error');
        zeroError.classList.remove('hidden');
    }
    calculateTipAmount();
};

const handleBill = () => {
    if (billEntry.value) {
        billEntry.value = billEntry.value.replace(',', '.');
        billValue = Number(billEntry.value);
    }
    calculateTipAmount();
};

const removeSelectedDigit = () => {
    digits.forEach((el) => {
        el.classList.remove('selected');
    });
};

const resetAllEntries = () => {
    billEntry.value = null;
    peopleEntry.value = null;
    customEntry.value = null;
    tipResult.innerHTML = '$0.00';
    totalResult.innerHTML = '$0.00';
    zeroError.classList.add('hidden');
    peopleEntry.classList.remove('border-error');
};

const removeCustomEntry = () => {
    customEntry.value = null;
};

billEntry.addEventListener('input', handleBill);
digits.forEach((el) => {
    el.addEventListener('click', handleDigit);
});
customEntry.addEventListener('input', handleCustomTip);
peopleEntry.addEventListener('input', handlePeople);
resetButton.addEventListener('click', () => {
    resetAllEntries();
    removeSelectedDigit();
});
